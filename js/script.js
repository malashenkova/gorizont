$(document).ready(function () {
  if ($(this).scrollTop() > 1) {
    $(".page__header").addClass("fixed");
  } else {
    $(".page__header").removeClass("fixed");
  }
  let scrollPos = $(this).scrollTop();
  $(window).scroll(function () {
    let newScrollPos = $(this).scrollTop();
    if ($(this).scrollTop() > $(".page__header").outerHeight()) {
      $(".page__header").addClass("fixed");
    } else {
      $(".page__header").removeClass("fixed");
    }
    if (
      newScrollPos < scrollPos &&
      newScrollPos > $(".page__header").outerHeight()
    ) {
      $(".page__header").addClass("top");
      scrollPos = newScrollPos;
    } else {
      $(".page__header").removeClass("top");
      scrollPos = newScrollPos;
    }
  });

  // красивый select
  if ($("select").length) {
    $("select").select2({
      minimumResultsForSearch: Infinity,
      // closeOnSelect: false,
    });
  }

  // маска для инаупов
  if ($("[data-inputmask]").length) {
    $(":input").inputmask();
  }
  $("input").on("change", function () {
    if ($(this).val().length) {
      $(this).addClass("not-empty");
    } else {
      $(this).removeClass("not-empty");
    }
  });
  $('input[type="file"]').on('change', function() {
    var fileName = this.files[0].name;
    console.log('Имя файла: ' + fileName);
    $(this).parents('.js-drop-zone').find('.js-drop-zone-prompt').text(fileName)
  });
  // /маска для инпупов
  $(window).resize(function () {
    $(".js-select").select2({
      minimumResultsForSearch: Infinity,
    });
  });

  $('.vacancies__btn').on('click', function() {
    let name = $(this).parents('.js-vacancies-item').find('.vacancies__name').text()
    console.log($.trim(name))
    $('#career input').each(function() {
      if ($(this).attr('placeholder') === 'Желаемая должность') {
        $(this).val($.trim(name))
      }
    });
  })

  $('.js-closee-popup').on('click', function() {
    $.fancybox.close(true);
  })
  //  меню услуг

  $(".js-header-search__input").on("click", function () {
    $(".js-header-search").addClass("open");
  });
  $(".js-header-search__open").on("click", function () {
    $(".js-header-search").toggleClass("open");
  });
  $(".js-header-search__close").on("click", function () {
    $(".js-header-search").removeClass("open");
  });
  $(document).on("click", function (event) {
    if (!$(event.target).closest(".js-header-search").length) {
      $(".js-header-search").removeClass("open");
    }
  });

  $(".menu__li--has-submenu").on("mouseover", function () {
    $(".page__header").addClass("white-bg");
  });

  $(".menu").on("mouseleave", function () {
    $(".page__header").removeClass("white-bg");
  });

  $(".js-footer-menu-name").on("click", function () {
    $(this).toggleClass("open");
    $(this).parents(".footer__menu").find(".js-footer-menu").slideToggle();
  });

  $(".solutions__item").on("mouseover", function () {
    let image = $(this).attr("data-src");
    $(".solutions").css("background-image", "url(" + image + ")");
  });
  $(".solutions__item").each(function () {
    let image = $(this).attr("data-src");
    $(this).css("background-image", "url(" + image + ")");
  });

  $(".mobile-menu__item.has-children").on("click", function () {
    $(this).toggleClass("open");
    // $(this).find(".mobile-menu__subul").slideToggle();
  });

  $(".js-mobile-menu-opener").on("click", function () {
    $('body').toggleClass('fixed')
    $('.page__header').toggleClass('menu-open')
    $(this).parents(".mobile-menu").toggleClass("open");
    $(".js-mobile-menu-inside").slideToggle();
  });

  if ($(".solutions").length) {
    let image = $(".solutions__item:first-child").attr("data-src");
    $(".solutions").css("background-image", "url(" + image + ")");
  }

  if ($("#map").length) {
    ymaps.ready(function () {
      console.log("map");
      var myMap = new ymaps.Map(
          "map",
          {
            center: [55.79232, 38.642174],
            zoom: 9,
          },
          {
            controls: [],
          }
        ),
        MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
          '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
        ),
        myPlacemark1 = new ymaps.Placemark(
          [55.747619, 37.651793],
          {},
          {
            iconLayout: "default#image",
            iconImageHref:
              "file:///D:/фронтенд/горизонт%20покрытий/img/icons/map.svg",
            iconImageSize: [44, 54],
            iconImageOffset: [-22, -54],
          }
        ),
        myPlacemark2 = new ymaps.Placemark(
          [55.981486, 37.414909],
          {},
          {
            iconLayout: "default#image",
            iconImageHref:
              "file:///D:/фронтенд/горизонт%20покрытий/img/icons/map.svg",
            iconImageSize: [44, 54],
            iconImageOffset: [-22, -54],
          }
        ),
        myPlacemark3 = new ymaps.Placemark(
          [55.679138, 37.263663],
          {},
          {
            iconLayout: "default#image",
            iconImageHref:
              "file:///D:/фронтенд/горизонт%20покрытий/img/icons/map.svg",
            iconImageSize: [44, 54],
            iconImageOffset: [-22, -54],
          }
        ),
        myPlacemark4 = new ymaps.Placemark(
          [55.676748, 37.893328],
          {},
          {
            iconLayout: "default#image",
            iconImageHref:
              "file:///D:/фронтенд/горизонт%20покрытий/img/icons/map.svg",
            iconImageSize: [44, 54],
            iconImageOffset: [-22, -54],
          }
        ),
        myPlacemark5 = new ymaps.Placemark(
          [55.78445, 38.444858],
          {},
          {
            iconLayout: "default#image",
            iconImageHref:
              "file:///D:/фронтенд/горизонт%20покрытий/img/icons/map.svg",
            iconImageSize: [44, 54],
            iconImageOffset: [-22, -54],
          }
        );

      myMap.geoObjects
        .add(myPlacemark1)
        .add(myPlacemark2)
        .add(myPlacemark3)
        .add(myPlacemark4)
        .add(myPlacemark5);

      // Устанавливаем границы карты, чтобы все метки были видны
      var bounds = myMap.geoObjects.getBounds();
      myMap.setBounds(bounds, {
        checkZoomRange: true
      });
    });
  }


  if ($(".js-news-slider").length) {
    var swiper = new Swiper(".js-news-slider", {
      loop: true,
      spaceBetween: 12,
      slidesPerView: "auto",
      pagination: {
        el: ".js-news-slider-pagination",
      },
      navigation: {
        nextEl: ".js-news-slider-next",
        prevEl: ".js-news-slider-prev",
      },
      breakpoints: {
        600: {
          spaceBetween: 15,
        },
        1240: {
          spaceBetween: 20,
        },
      },
    });
  }

  $(".js-faq-question").on("click", function () {
    $(this).parents(".js-faq-item").toggleClass("open");
    $(this).parents(".js-faq-item").find(".js-faq-answer").slideToggle("open");
  });

  $(".coatings__plus").on("click", function () {
    $(this).parents(".coatings__item").toggleClass("open");
    $(this).parents(".coatings__item").find(".coatings__item--children").slideToggle("");
  });

  

  $(".js-vacancies-show").on("click", function () {
    $(this).parents(".js-vacancies-item").toggleClass("open");
    $(this)
      .parents(".js-vacancies-item")
      .find(".js-vacancies-hide")
      .slideToggle("open");
  });

  $('input[type="file"]').on("change", function () {
    var fullPath = $(this).val();
    var pathSeparator = fullPath.indexOf("\\") !== -1 ? "\\" : "/";
    var parts = fullPath.split(pathSeparator);
    var fileName = parts[parts.length - 1];
    if ($(this).parents(".file-input").length) {
      $(this).parents(".file-input").find("span").text(fileName);
    }
  });

  if ($(".js-review-slider").length) {
    var swiper = new Swiper(".js-review-slider", {
      loop: true,
      spaceBetween: 12,
      slidesPerView: "auto",
      pagination: {
        el: ".js-review-slider-pagination",
      },
      navigation: {
        nextEl: ".js-review-slider-next",
        prevEl: ".js-review-slider-prev",
      },
      breakpoints: {
        600: {
          spaceBetween: 15,
          slidesPerView: "auto",
        },
        1240: {
          spaceBetween: 20,
          slidesPerView: 3,
        },
      },
    });
  }

  if ($(".js-work-slider").length) {
    var swiper = new Swiper(".js-work-slider", {
      loop: true,
      spaceBetween: 12,
      slidesPerView: 1,
      pagination: {
        el: ".js-work-slider-pagination",
      },
      navigation: {
        nextEl: ".js-work-slider-next",
        prevEl: ".js-work-slider-prev",
      },
      breakpoints: {
        600: {
          spaceBetween: 15,
          slidesPerView: 2,
        },
        1240: {
          spaceBetween: 20,
          slidesPerView: 4,
        },
      },
    });
  }

  $(".js-openblock-toggle").on("click", function () {
    $(this).parents(".js-openblock").toggleClass("open");
    $(this)
      .parents(".js-openblock")
      .find(".js-openblock-content")
      .slideToggle();
  });

  $(".js-tab-selected").on("click", function () {
    $(this).parents(".js-tabs").toggleClass("open");
  });
  $(".js-tab-item").on("click", function () {
    $(this).parents(".js-tabs").removeClass("open");
    $(".js-tab-item.active").toggleClass("active");
    $(this).addClass("active");
    $(this).parents(".js-tabs").find(".js-tab-selected").text($(this).text());
  });

  $(".tags__item--last").on("click", function () {
    $(".tags__item").show();
    $(this).hide();
  });

  $(".js-career-filter__opener").on("click", function () {
    $(".js-career-filter__inside").slideDown();
  });
  $(".js-career-filter__cross").on("click", function () {
    $(".js-career-filter__inside").slideUp();
  });

  if ($(".js-service-steps").length) {
    let bigHeight = $(".service-steps__right").outerHeight();
    let sliderHeight = bigHeight - $(".service-steps__textblock").outerHeight();
    $(".service-steps__sliderbox").css("height", sliderHeight);

    var swiper = new Swiper(".js-service-steps", {
      direction: "vertical",
      slidesPerView: 1,
      spaceBetween: 0,
      speed: 200,
      pagination: {
        el: ".js-service-steps__pagination",
        type: "progressbar",
      },
      mousewheel: {
        releaseOnEdges: true,
        eventsTarget: ".service-steps__sliderbox",
      },
      on: {
        slideNextTransitionEnd: function (swiper) {
          let lastBottom =
            $(".service-steps__slide:last-child").offset().top +
            $(".service-steps__slide:last-child").outerHeight();

          let boxBottom =
            $(".service-steps__sliderbox").offset().top +
            $(".service-steps__sliderbox").outerHeight();

          if (lastBottom <= boxBottom) {
            swiper.params.speed = 0;
          } else {
            swiper.params.speed = 200;
          }
        },

        slideNextTransitionStart: function (swiper) {
          let lastBottom =
            $(".service-steps__slide:last-child").offset().top +
            $(".service-steps__slide:last-child").outerHeight();

          let boxBottom =
            $(".service-steps__sliderbox").offset().top +
            $(".service-steps__sliderbox").outerHeight();
          if (lastBottom <= boxBottom) {
            let activeTop =
              $(".service-steps__slide:last-child").offset().top +
              $(".service-steps__slide:last-child").outerHeight();
            if ($(".service-steps__slide.swiper-slide-next").length) {
    
              activeTop = $(".service-steps__slide.swiper-slide-next").offset()
                .top;
            }
            let topNeeded =
              activeTop - $(".service-steps__sliderbox").offset().top;
            $(".js-service-steps").css("top", topNeeded + "px");
            console.log(lastBottom, boxBottom, topNeeded, activeTop);
          } else {
            swiper.params.speed = 200;
          }
        },
        slidePrevTransitionEnd: function (swiper) {
          if (
            $(".service-steps__slide:first-child").offset().top >=
            $(".service-steps__sliderbox").offset().top
          ) {
            let activeTop = $(".service-steps__slide:first-child").offset().top;
            let topNeeded = 0;

            if ($(".service-steps__slide.swiper-slide-prev").length) {
              activeTop = $(".service-steps__slide.swiper-slide-prev").offset()
                .top;
              topNeeded =
                activeTop - $(".service-steps__sliderbox").offset().top;
            }

            swiper.params.speed = 0;
            $(".js-service-steps").css("top", topNeeded + "px");
          } else {
            swiper.params.speed = 200;
          }
        },
      },
    });
  }


  if ($(".js-partners").length) {
    var swiper = new Swiper(".js-partners", {
      loop: true,
      spaceBetween: 12,
      slidesPerView: 1,
      breakpoints: {
        500: {
          spaceBetween: 15,
          slidesPerView: 2,
        },
        700: {
          spaceBetween: 20,
          slidesPerView: 3,
        },
        1240: {
          spaceBetween: 20,
          slidesPerView: 3,
        },
        1440: {
          spaceBetween: 20,
          slidesPerView: 4,
        },
      },
    });
  }

  if ($(".js-types-slider").length) {
    var swiper = new Swiper(".js-types-slider", {
      loop: true,
      spaceBetween: 12,
      slidesPerView: "auto",
      breakpoints: {
        600: {
          spaceBetween: 15,
        },
        1240: {
          spaceBetween: 20,
        },
      },
    });
  }

  if ($(".js-simple-reviews").length) {
    var swiper = new Swiper(".js-simple-reviews", {
      loop: true,
      spaceBetween: 12,
      slidesPerView: 1,
      breakpoints: {
        600: {
          spaceBetween: 15,
          slidesPerView: 2,
        },
        1240: {
          spaceBetween: 20,
          slidesPerView: 4,
        },
      },
    });
  }

  if ($(".js-content-slider").length) {
    var swiper = new Swiper(".js-content-slider", {
      loop: true,
      spaceBetween: 12,
      pagination: {
        el: ".js-content-slider-pagination",
      },
      navigation: {
        nextEl: ".js-content-slider-next",
        prevEl: ".js-content-slider-prev",
      },
     
    });
  }

  if ($(".js-history").length) {

    var swiper = new Swiper(".js-history", {
      slidesPerView: 1,
      spaceBetween: 0,
      speed: 200,
      pagination: {
        el: ".js-history__pagination",
        type: "progressbar",
      },
      mousewheel: {
        releaseOnEdges: true,
        eventsTarget: ".history",
      },
      on: {
        slideNextTransitionEnd: function (swiper) {

          let lastRigth =
            $(".history__slide:last-child").offset().left +
            $(".history__slide:last-child").outerWidth();

          let boxRigth =
            $(".history__sliderbox").offset().left +
            $(".history__sliderbox").outerWidth();

          if (lastRigth <= boxRigth) {
            swiper.params.speed = 0;
          } else {
            swiper.params.speed = 200;
          }

        },

        slideNextTransitionStart: function (swiper) {

          $(".history__slide.swiper-slide-prev").addClass('red')

          let lastRigth =
            $(".history__slide:last-child").offset().left +$(".history__slide:last-child").outerWidth();

          let boxRigth =
            $(".history").offset().left +
            $(".history").outerWidth();
            console.log(lastRigth , boxRigth)
          if (lastRigth <= boxRigth) {
            let activeleft =
              $(".history__slide:last-child").offset().left +
              $(".history__slide:last-child").outerWidth();

              activeleft = $(".history__slide.swiper-slide-next").offset()
                .left;

            
            let leftNeeded =
              activeleft - $(".history").offset().left;
            $(".js-history").css("left", leftNeeded + "px");
          } else {
            swiper.params.speed = 200;
          }
        },
        slidePrevTransitionEnd: function (swiper) {
          $(".history__slide.swiper-slide-next").removeClass('red')
          $(".history__slide.swiper-slide-active").removeClass('red')
          if (
            $(".history__slide:first-child").offset().left >=
            $(".history").offset().left
          ) {
            let activeleft = $(".history__slide:first-child").offset().left;
            let leftNeeded = 0;

            if ($(".history__slide.swiper-slide-prev").length) {
              activeleft = $(".history__slide.swiper-slide-prev").offset()
                .left;
                leftNeeded =
                activeleft - $(".history").offset().left;
            }

            swiper.params.speed = 0;
            $(".js-history").css("left", leftNeeded + "px");
          } else {
            swiper.params.speed = 200;
          }
        },
      },
    });
  }

  if ($(".js-production-slider").length) {
    var slider = new Swiper(".js-production-slider", {
      loop: true,
      scrollbar: {
        el: ".js-production-scrollbar",
      },
      pagination: {
        el: ".js-production-pagination",
        clickable: true,
      },
    });
    
    const thumbs = new Swiper('.js-production-thumbs', {
      loop: true,
      slidesPerView: 'auto',
      spaceBetween: 20,
      watchSlidesProgress: true,
      slideToClickedSlide: true,
    });

    slider.controller.control = thumbs;
    thumbs.controller.control = slider;
    
    var currentUrl = $(location).attr('href');

    var hash = currentUrl.split('#')[1];

    var $slider = $('.js-production-slider');
    var needSlide = $slider.find('.swiper-slide#'+ hash)
    var needSlideHtml = needSlide.html()
    var firstSlide = $slider.find('.swiper-slide').first()
    var firstSlideHtml = firstSlide.html()
    needSlide.html(firstSlideHtml)
    firstSlide.html(needSlideHtml)

    document.querySelectorAll('.js-production-thumbs .swiper-slide').forEach((slide, index) => {
      let id = slide.getAttribute('data-slide');
      let $slider = $('.js-production-slider');
      let ind = $slider.find('.swiper-slide#'+ id).index();
      if (ind<9) {
        slide.querySelector('.production-slider__thumb-num').textContent = `0${ind + 1}`;
      } else {
        slide.querySelector('.production-slider__thumb-num').textContent = `${ind + 1}`;
      }
    });
  }

  if ($("#info-map").length) {
    var placemarks = [];
    ymaps.ready(function () {
      console.log("info-map");
      var myMap = new ymaps.Map(
        "info-map",
        {
          center: [55.79232, 38.642174],
          zoom: 9,
        },
        {
          controls: [],
        }
      );
      var myCollection = new ymaps.GeoObjectCollection();
  
      $(".js-map-placemark").each(function () {
        let lat = $(this).find('.js-map-coords').attr('data-lat')
        let long = $(this).find('.js-map-coords').attr('data-long')
        var placemark = new ymaps.Placemark(
          [parseFloat(lat), parseFloat(long)],
          {},
          {
            iconLayout: "default#image",
            iconImageHref: "file:///D:/фронтенд/горизонт%20покрытий/img/icons/map.svg",
            iconImageSize: [44, 54],
            iconImageOffset: [-22, -54],
          }
        );
        placemarks.push(placemark);
        myCollection.add(placemark);
        
      });
      myMap.geoObjects.add(myCollection);
      var bounds = myMap.geoObjects.getBounds();
      myMap.setBounds(bounds, {
        checkZoomRange: true
      });


      placemarks.forEach(function (placemark) {
      
        placemark.events.add('click', function (e) {
          placemark.options.set('iconImageHref', 'file:///D:/фронтенд/горизонт%20покрытий/img/icons/map-active.svg');
          console.log(placemark.geometry.getCoordinates())
          let lat = placemark.geometry.getCoordinates()[0]
          let long = placemark.geometry.getCoordinates()[1]
          let element = $(`[data-lat="${lat}"][data-long="${long}"]`).parent('.js-map-placemark');

          $('.js-map-placemark').hide()
          element.addClass('active').show()

          placemarks.forEach(function (otherPlacemark) {
            if (otherPlacemark !== placemark) {
                otherPlacemark.options.set('iconImageHref', 'file:///D:/фронтенд/горизонт%20покрытий/img/icons/map.svg');
            }
          });

        })
      })

    });

    $('.js-map-close').on('click', function(){
      $(this).parents('.js-map-placemark').removeClass('active').hide();
      placemarks.forEach(function (otherPlacemark) {
       
            otherPlacemark.options.set('iconImageHref', 'file:///D:/фронтенд/горизонт%20покрытий/img/icons/map.svg');
        
      });
    })
  }
  
  if ($(".js-awards").length) {
    var slider = new Swiper(".js-awards", {
      slidesPerView: 'auto'
    });
  }

  if ($(".js-text_with_video__slider").length) {
    var slider = new Swiper(".js-text_with_video__slider", {
      loop: true,
      pagination: {
        el: ".js-text_with_video__pagination",
      },
    });
  }

  $('.js-big-table table th').each(function(index) {
    var thText = $(this).text();
    $('table td:nth-child(' + (index + 1) + ')').each(function() {
      $(this).prepend('<span class="big-table__name">' + thText + ' </span>');
    });
  });

});
